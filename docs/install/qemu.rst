.. _QEMU:

QEMU
####

This document describes how to build and run for QEMU target and we suppose that
you have been already fulfilled the steps mentioned at the
:ref:`getting_started` page. If not, start there before continuing.

Building
********
Since we're using a virtual python enviroment, start by source that

.. code-block:: bash

    $ source <workspace root>/.pyvenv/bin/activate

.. note::
   The ``source`` above has to be done **once** everytime you spawn a new shell.

Start the build, this will probably take several hours on a normal desktop
computer the first time you're building it with nothing in the cache(s). The TRS
is based on various Yocto layers and if you don't have your ``DL_DIR`` and
``SSTATE_DIR`` set as an environment variable, those will be set to
``$HOME/yocto_cache`` by default.

.. code-block:: bash

    $ cd <workspace root>
    $ make

Run - First boot
****************
Once the build has been completed, you can run it on your host machine with
QEMU.

.. code-block:: bash

    $ cd <workspace root>
    $ make run

At the first boot you will be dropped into a U-Boot shell. There we need to
point to a couple of efi binaries


.. code-block:: bash

    => efidebug boot add -b 1 BootLedge virtio 0:1 efi/boot/bootaa64.efi -i virtio 0:1 ledge-initramfs.rootfs.cpio.gz -s 'console=ttyAMA0,115200 console=tty0 root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60' ; efidebug boot order 1 ; bootefi bootmgr

.. note::
    To quit QEMU, press ``Ctrl-A x`` (alternatively kill the qemu-system-aarch64 process)

Quit QEMU and continue with the next step.

Run - After first boot
**********************
.. code-block:: bash

    $ cd <workspace root>
    $ make run

If everything goes as expected, you will presented login message and a login
prompt. The login name is ``ewaol`` as depicted below.

.. code-block:: bash

    ledge-secure-qemuarm64 login: ewaol
    ewaol@ledge-secure-qemuarm64:~$

FAQ
***

..
  [NEEDS_TO_BE_FIXED] - Empty section, see Rockpi4 as an example

TBD
